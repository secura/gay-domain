Starting on 10 February 2020 the .gay domains have become available for registration.

Get in contact with the gay community amongst your customers and target audiences. Use the upcoming .gay top level domain for targeted marketing of the financially strong gay community, especially in the US market. Show that you care.
It is recommended to you and your clients to protect company names, trademarks and service marks against registration by others and potentially harmful use.
Sunrise period starts on 10 February 2020. There will be 2 phases at the Sunrise period.

Sunrise Period I
Sunrise Period I domain registrations are available to trademark owners who have their trademarks registered with the Trademark Clearinghouse. Sunrise Period I will close on 31 March 2020.

Sunrise Period II
Sunrise Period II starts on 6 April 2020. All trademark owners can apply for domain registrations. 
A trademark registration with the Trademark Clearingshouse is not necessary at this phase. 

Early Access Period
Your company name is not registered as a trademark or your trademark, which you use, is unregistered. The Early Access Period is made for you at this case. You can register your domain name prior the others, which use the General Availabilty. The Early Access Period works as a "dutch auction". Registration prices start higher and then decrease daily. If you are interested at prices and conditions, you should send an e-mail to secura@domainregistry.de .

General Availability
General Availability will start on 20 May 2020.


The .gay registry donates 20% of its income to the partner organizations GLAAD and CenterLink.

The main arguments for registering a .gay domain are:

- The .gay domains are the domains of the gay community for the gay community

- The .gay domains are self-explanatory domains. What is gay, fits best on a website with a .gay domain. A website address with a .gay domain is shorter and more memorable because "gay" no longer has to appear at the left of the dot.    

- Every registration of a .gay domain is a commitment to diversity and an open society

- There are good reasons to register a .gay domain for commercial reasons. Anyone who registers a .gay domain picks up the community where it is located.


The relationship between better ranking and the new top-level domains was proved by a study of Searchmetrics for .berlin domains. Websites with .berlin domains frequently place better than websites with .de domains and .com domains in regional searches with Google. The result of the study by Searchmetrics can be summarized as follows:

"42% of searches show that .berlin domains rank better locally."

The study of Total Websites in Houston shows that the results by Searchmetrics can be generalized to all new top level domains, including the .gay domains: It was proved that Google uses the domain endings of the New Top Level Domains as a key element for the assessment of domains. Total Websites draws as a conclusion:

"It is clear that the new top-level domains improve the ranking in search engines." 


  Mike Muller

https://www.domainregistry.de/gay-domain.html
